﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Print "Hello" and name to screen
            Console.WriteLine("Hello");
            Console.WriteLine("Phoebe-Rianna Omundsen");
            Console.ReadLine();

            //Print the sum of 2 numbers
            int x = 74;
            int y = 36;

            Console.WriteLine(x + y);
            Console.ReadLine();

            //Divide 2 numbers and print on screen
            int a = 50;
            int b = 3;

            Console.WriteLine(a / b);
            Console.ReadLine();

            //Take 2 numbers as input and multiply
            Console.Write("Please enter a whole number:");
            var num1 = int.Parse(Console.ReadLine());
            Console.Write("Please enter a whole number:");
            var num2 = int.Parse(Console.ReadLine());
            Console.WriteLine($"{num1} x {num2} = {num1 * num2}");
            Console.ReadLine();

            //take a number as input and prints its multiplication table up to 10
            Console.Write("Please input a whole number:");
            var input1 = int.Parse(Console.ReadLine());
            
            for (int i = 1; i <= 10; i++)
                {
                int j = input1 * i;
                Console.WriteLine($"{input1} x {i} = {j}");
                }
            Console.ReadLine();

            //Print the area and perimeter of a circle
            var radius = 7.5;
            var pi = 3.14592653589793238;

            Console.WriteLine($"Radius = {radius}");
            Console.WriteLine($"Perimeter is = {2*pi*radius}");
            Console.WriteLine($"Area is = {pi*radius*radius}");
            Console.ReadLine();

            //swap 2 variables
            var a1 = 5;
            var b1 = 8;
            var c1 = 0;

            Console.WriteLine($"Var a1 = {a1}, Var b1 = {b1}");

            a1 = b1;
            b1 = a1;

            Console.WriteLine($"Var a1 = {a1}, Var b1 = {b1}");
            Console.ReadLine();



        }

    }
}
